#scraping.py

#Library imports
import requests
from bs4 import BeautifulSoup
import linecache
import sys
from tabulate import tabulate

#variable
#print (dir(BeautifulSoup))
hn = 'https://news.ycombinator.com/rss'
article_list= []
#Define PrintException
def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

#Probing HackerNews RSS
def hackernews_rss(hn):
    try:
        r = requests.get(hn)
        soup = BeautifulSoup(r.content, features='xml')
        #Getting articles
        articles = soup.findAll('item')
           
        for a in articles:
            title = a.find('title').text
            link = a.find('link').text
            published = a.find('pubDate').text
            article = {
                title,
                link,
                published
            }
            article_list.append(str(article) +'\n')
        def save_function(article_list):
         with open('articles.txt', 'w') as f:
            # create data           
            for a in article_list:
                f.write(a)
            f.close()
        #return save_function(article_list)
        return print (article_list)
    except:
       PrintException()
print ('Starting scraping')
hackernews_rss(hn)
print('Finished scraping')
#Begin of day : March 21, 2023
#Scraping to a file